import { StyleSheet, Dimensions } from 'react-native';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight= Dimensions.get('window').height;
const headerHeight = deviceHeight / 12;
const deviceHeightMinusHeader = deviceHeight - headerHeight;
const gridViewHeight = ((deviceHeightMinusHeader) / 7) * 5;
const gridViewWidth = deviceWidth / 2;
const drawerHeaderHeight = deviceHeight / 19;

module.exports = StyleSheet.create({

container: {
    flex: 1,
    backgroundColor: '#333',
  },

  drawerContainer: {
    alignItems: 'flex-end',
    height: deviceHeight,
    width: ((deviceWidth * 2) / 7) + 30,
    position: 'absolute',
    left: (deviceWidth - ((deviceWidth * 2) / 7)) - 30,
  },

  headerLayout: {
    marginBottom: 1, 
    backgroundColor: '#fff'
  },

  headerText: {
    color: 'black', 
    fontSize: 20, 
    textAlign: 'left', 
    margin: 7,
  },

  headerIcon: {
    margin: 7,
  }

});
