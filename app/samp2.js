/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator
} from 'react-native';
import styles from './style/style.js';
import Drawer from './drawer.js';

import { Card, ListItem, Button } from 'react-native-elements';

export default class Samp extends Component<{}> {

  constructor(props){
    super(props);
    this.state = {
      clubsObj: []
    };
  }

  render() {
   
    return (
      <View style={{flex: 1,backgroundColor: 'white'}} >
        <View style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0, 
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'row'
        }}>


      <Card
          image={require('./assets/images/asd.jpg')}
          title='Our Club'
          >
          <Text style={{width: 250,marginBottom: 10}}>
            The idea with React Native Elements is more about component structure than actual design.
          </Text>
          <Button
            icon={{name: 'code'}}
            backgroundColor='#03A9F4'
            onPress={() => {this.props.changeComponent('Main')}}
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title='VIEW NOW' />
        </Card>

        <Card
          title='Our Fitness Coaches'
          image={require('./assets/images/asd.jpg')}
          >
          <Text style={{width: 250,marginBottom: 10}}>
            The idea with React Native Elements is more about component structure than actual design.
          </Text>
          <Button
            icon={{name: 'code'}}
            backgroundColor='#03A9F4'
            onPress={() => {this.props.changeComponent('Mains3')}}
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title='VIEW NOW' />
        </Card>

        <Card
          title='Our Community'
          image={require('./assets/images/asd.jpg')}
          >
          <Text style={{width: 250,marginBottom: 10}}>
            The idea with React Native Elements is more about component structure than actual design.
          </Text>
          <Button
            icon={{name: 'code'}}
            backgroundColor='#03A9F4'
            onPress={() => {this.props.changeComponent('Main')}}
            buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
            title='VIEW NOW' />
        </Card>
        </View>
      </View>
    );
  }
}

