/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, PropTypes } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ActivityIndicator
} from 'react-native';
import styles from './style/style.js';
import Drawer from './drawer.js';
import Main from './samp.js';
import Mains from './samp2.js';
import Mains3 from './samp3.js';
import GLOBAL from './utils/global.js';

export default class App extends Component<{}> {

  constructor(props){
    super(props);
    this.state = {
      clubsObj: [],
      componentSelected: 'Main',
    };
  }


  changeComponent = (component) => {
    this.setState({
      componentSelected: component  
    })
  }

  renderComponent(component){
    if (component == 'Main') {
        return <Main changeComponent={this.changeComponent} />
    }else if (component == 'Mains'){
        return <Mains changeComponent={this.changeComponent} />
    }else if (component == 'Mains3'){
        return <Mains3 changeComponent={this.changeComponent} />
    }
  }

  render() {
   GLOBAL.selected = this;
    return (
      <View style={styles.container}>
      <View style={{flex : 1}}>
       {this.renderComponent(this.state.componentSelected)}
      </View>
        <Drawer parent={this}/>
      </View>
    );
  }
}

