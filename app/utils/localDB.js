'use strict';

var React = require('react-native');
import SQLite from 'react-native-sqlite-storage';
let db = SQLite.openDatabase({name : "fitness.db", createFromLocation : 1}, openCB,errorCB); 
SQLite.enablePromise(true);

	var tblClubs = "ClubsTable";

	var openCB = () => {
       console.log("open this");
  	}

	var closeCB = () => {
	   console.log("close");
	}

	var  errorCB = (err) => {
       console.log("error: ",err);
       return false;
    }


var GetClubsBranches = async () => {

let row = [];
    let query = ['Select * from ' + tblClubs];
       await db.transaction(tx => {
       return Promise.all(query.map(async (q) => {
          try {
            await tx.executeSql(q, [],(tx, results) => {
                   console.log("Query completed");
                
                  var len = results.rows.length;
                  for (let i = 0; i < len; i++) {
                    row.push(results.rows.item(i).Branches);
                    // row[i] = {
                    //   id: results.rows.item(i).id,
                    //   branches: results.rows.item(i).Branches,
                    // };
                 }
        
                },errorCB, function(){

                });
            console.log('Query', q, 'Executed. results:', results);

          } catch(err) {
            console.log('Something went wrong while executing query', q, 'error is', err);
          }
        }));
      });
return row;

}


exports.GetClubsBranches = GetClubsBranches;
