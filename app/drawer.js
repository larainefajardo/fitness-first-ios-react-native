/**
 * Author: Laraine Fajardo
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  Animated,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import styles from './style/style.js';
import localDB from './utils/localDB.js';
import App from './app.js';
import GLOBAL from './utils/global.js';

import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';
import * as Animatable from 'react-native-animatable';
import SQLite from 'react-native-sqlite-storage';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const initialDrawerLeft = 0;
const drawerWidthMinusCollapseButton = ((deviceWidth * 2) / 7) - 30;


export default class Drawer extends Component<{}> {

  constructor(props){

    super(props);
    this.state = {
      isActive: false,
      activeSection: false,
      isCollapsed: false,
      collapseAnimation : new Animated.Value(parseFloat(initialDrawerLeft)),
      collapseFadeAnimation : new Animated.Value(0.8),
      isLoading: true,
      clubsObj: [],
      count: 0
    };
  }

  async componentWillMount(){
    var clubs = [];
    clubs = await localDB.GetClubsBranches();
    this.setState({clubsObj : clubs, isLoading: false});
  }

  _setSection(section) {
    this.setState({ activeSection: section });
  }

   _renderHeader(section, i, isActive) {

      return (
        <Animatable.View duration={1200} transition="backgroundColor" style={[styles.headerLayout, {flexDirection: 'row'}]}>

          <Text style={styles.headerText}>{section.title}</Text>

          <View style={{flex: 1, alignItems: 'flex-end', justifyContent:'center'}}>

           <Image source={isActive == true ? require('./assets/images/substract.png') : require('./assets/images/add.png')} style={[styles.headerIcon, {width: 15, height: 15,}]}/>
          </View>

        </Animatable.View>
      );


  }

  _renderContent = (section, i, isActive) => {

    if(section.title == 'Clubs'){
      return (
        <Animatable.View
        duration={1200}
        collapseAnimation={isActive ? 'zoomInUp' : false}
        style={{ backgroundColor: 'transparent', flex: 1, marginBottom: 1}}>

        {this.contentLayout(section.parent)}

        </Animatable.View>
      );

    } else if(section.title == 'Fitness Welcome'){

      return (
        <Animatable.View
        duration={1200}
        collapseAnimation={isActive ? 'zoomInUp' : false}
        style={{ backgroundColor: 'transparent', flex: 1 }}>

        <Text style={{color: 'white'}}>asd</Text>

        </Animatable.View>
      );

    } else if(section.title == 'CustomFit'){

      return (
        <Animatable.View
        duration={1200}
        collapseAnimation={isActive ? 'zoomInUp' : false}
        style={{flex: 1, backgroundColor: 'transparent' }}>

        <Text style={{color: 'white'}}>asd</Text>

        </Animatable.View>
      );

    }
  }

  toggleDrawer(){

      currentIsCollapsed = !this.state.isCollapsed;

      this.setState({
          isCollapsed : !this.state.isCollapsed
      });

        if(currentIsCollapsed) {

          this.state.collapseAnimation.setValue(parseFloat(initialDrawerLeft));  //Step 3

          Animated.spring(
            this.state.collapseAnimation,
            {
                toValue: parseFloat(initialDrawerLeft) - parseFloat(drawerWidthMinusCollapseButton + 30),

            }
          ).start();

          this.state.collapseFadeAnimation.setValue(0.8);

          Animated.timing(
            this.state.collapseFadeAnimation,
            {
              toValue: 0,
              duration: 300,
            }
          ).start();

        } else {

          this.state.collapseAnimation.setValue(parseFloat(initialDrawerLeft) - parseFloat(drawerWidthMinusCollapseButton));  //Step 3

          Animated.spring(
            this.state.collapseAnimation,
            {
                toValue: parseFloat(initialDrawerLeft),
            }
          ).start();

          this.state.collapseFadeAnimation.setValue(0);

          Animated.timing(
            this.state.collapseFadeAnimation,
            {
              toValue: 0.8,
              duration: 500,
            }
          ).start();

        }
  }

  contentLayout(that){
    return this.state.clubsObj.map(function(i){
      return(
        <TouchableOpacity key={i} onPress={() =>{that.props.parent.changeComponent('Mains')}}>
         <View key={i} style={{flex: 1, flexDirection: 'row', margin: 3, left: 10, alignItems: 'center'}}>
           <Text style={{fontSize: 8, right: 2}}>■ </Text>
           <Text style={{color: 'black',  fontSize: 17}}>{i}</Text>
         </View>
        </TouchableOpacity>
        );
      });
  }

  render() {
    const SECTIONS = [
      {
        title: 'Clubs',
        parent: this,
      },
      {
        title: 'Fitness Welcome',
        parent: this,
      },
      {
        title: 'CustomFit',
        parent: this,
      },
      {
        title: 'Bioscore',
        parent: this,
      },
      {
        title: 'Group Exercise Playlist',
        parent: this,
      },
      {
        title: 'Corporate',
        parent: this,
      },
      {
        title: 'First Club',
        parent: this,
      },
      {
        title: 'Annual Campaigns',
        parent: this,
      }

    ];


    let collapseButton = null;
    if(this.state.isCollapsed){

      collapseButton = <TouchableOpacity onPress={ () => {this.toggleDrawer();}}>
        <Text>open</Text>

      </TouchableOpacity>
    } else {

      collapseButton = <TouchableOpacity onPress={ () => {this.toggleDrawer();}}>
          <Text>close</Text>

      </TouchableOpacity>
    }

    if(this.state.isLoading) {

      return (

        <View style={{alignItems: 'flex-end', height: deviceHeight, width: ((deviceWidth * 2) / 7), position: 'absolute', left: (deviceWidth - ((deviceWidth * 2) / 7))}}>
          <Collapsible collapsed={this.state.isCollapsed} style={{flex: 1, backgroundColor: '#000000', opacity: 0.8, alignItems: 'center'}}>

            <ActivityIndicator />

          </Collapsible>
        </View>
      );

    } else {

        return (

          <Animated.View style={[styles.drawerContainer, {left: this.state.collapseAnimation}]}>

            <View style={{flex: 1, flexDirection: 'row'}}>

              <Animated.View style={{flex: 1, backgroundColor: '#000000', alignItems: 'center'}}>
                <Accordion
                style={{flex:1, backgroundColor: 'white', width: drawerWidthMinusCollapseButton + 25}}
                sections={SECTIONS}
                duration={1200}
                renderHeader={this._renderHeader}
                renderContent={this._renderContent}
                activeSection={this.state.activeSection}
                onChange={this._setSection.bind(this)}
                />
              </Animated.View>


              <View style={{justifyContent: 'center'}}>

                {collapseButton}

              </View>

            </View>

          </Animated.View>
        );

    }

  }
}

